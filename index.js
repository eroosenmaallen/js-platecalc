/** @file       index.js
 *  @author     Eddie Roosenmaallen <silvermoon82@gmail.com>
 *  @date       July 2023
 *
 *  This module provides a simple weightlifting plate calculator.
 */

/** @typedef {Array<Number,Number|boolean>} Weight */

/** @typedef {Array<Weight>} Inventory */

/** Default inventory from which to allocate plates
 *  @type {Inventory}
 */
exports.defaultInventory = [
  [45.0, true],
  [25.0, true],
  [10.0, true],
  [ 5.0, true],
  [ 2.5, true],
];

/**
 *  Calculate the number and size of plates required to meet a given target
 *    weight.
 *
 *  Plate weight is multiplied by "ends"
 *  Bar weight is added once (input should be doubled for a pair of dumbbells)
 *
 *  @param  {number} target               Target weight
 *  @param  {Object} options              Additional options
 *  @param  {number} options.bar          Weight of the bar; default 0
 *  @param  {number} options.ends         Number of "ends" (usually 2 for
 *    barbell or 4 for a pair of dumbbells); default 2 (barbell)
 *
 *  @param  {Inventory} options.inventory Set of plates to draw upon;
 *    default exports.defaultInventory
 *
 *  @return {Inventory}                   Inventory listing plates and count of each (per end)
 */
exports.toPlates = function platecalc$$toPlates(target, options = {})
{
  const bar = options.bar  || 0;
  const ends = options.ends || 2;
  // copy-in the inventory so we can mutate it with impunity
  const inventory = [...(options.inventory || exports.defaultInventory)]
    .map(([size, count]) => ([size, count]));

  inventory.sort((a, b) => (b[0] - a[0]));

  let load = 0;
  let loadout = [];

  // console.debug('051: plates', { target, bar, ends, inventory });

  // for (each size plate)
  for (invPlate of inventory)
  {
    const weight = invPlate[0] * ends;
    const loadPlate = [invPlate[0], 0];

    // console.debug(`059: plate:`, invPlate);
    
    // Add this size plate as long as we have some and they will fit inside
    // our target weight
    while ((invPlate[1] === true || invPlate[1] >= ends)
      && (bar + weight + load) <= target)
    {
      loadPlate[1]++;
      load += weight;

      // If inventory has a limited number of plates, deduct them
      if (typeof invPlate[1] === 'number')
        invPlate[1] -= ends;
    }

    if (loadPlate[1])
      loadout.push(loadPlate);
  }

  return loadout;
}

/**
 *  Given an Inventory of plates and counts, return the total weight
 *
 *  @param  {Inventory} plates  Inventory of plates to tally up
 *  @param  {Object} options              Additional options
 *  @param  {number} options.bar          Weight of the bar; default 0
 *  @param  {number} options.ends         Number of "ends" (usually 2 for
 *    barbell or 4 for a pair of dumbbells); default 2 (barbell)
 *
 *  @return {number}         Total weight of plates and bar (if specified)
 */
exports.toWeight = function platecalc$$toWeight(plates, options = {})
{
  const bar = options.bar  || 0;
  const ends = options.ends || 2;

  let load = bar;

  for (plate of plates)
    load += plate[0] * (plate[1] * ends);

  return load;
}
