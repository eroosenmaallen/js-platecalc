# `platecalc`

Simple API for weight plate calculations.

Unit-agnostic; can be used for lbs or kg. Can be used in mixed-mode by
multiplying weights in pounds by 454 and kg by 1000, then working entirely in
grams.


## Usage

```javascript

const pc = require('platecalc');

// An Inventory is an array of [size, count] tuples
const myPlates = [
  [45, true],   // `true` indicates "a bunch, more than we need to count"
  [25, 4],      // If plates are limited, we can include the count
  [10, 8],
  [5, 8],
  [2.5, 4],
  [1.25, false] // we can also list but exclude sizes we know abvout but don't have
];

// Load a standard barbell to 85lbs:
const barbellLoad = pc.toPlates(85, { bar: 20, inventory: myPlates });

// barbellLoad = [ [ 25, 1 ], [ 5, 1 ], [ 2.5, 1 ] ]


// Load dumbbells to 38 (both hands total, so 4 ends to tally up)
const dbLoad = pc.toPlates(38, { ends: 4, inventory: myPlates });

// dbLoad = [ [ 5, 1 ], [ 2.5, 1 ] ]
// ... but wait, that seems wrong?


// Calculate the weight for a given set of plates:
const dbWeight = pc.toWeight(dbLoad, { ends: 4 });

// dbWeight = 30
// Okay, that makes sense now! We have plates down to 2.5lbs


// For t-handle rows or similar, we only load one end:
const tHandleLoad = db.toWeight(195, { ends: 1, inventory: myPlates });

```
